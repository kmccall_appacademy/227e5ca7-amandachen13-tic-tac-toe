class Board

  def initialize(grid = nil)
    if grid
      @grid = grid
    else
      @grid = [
        [nil, nil, nil],
        [nil, nil, nil],
        [nil, nil, nil]
      ]
    end
  end

  attr_accessor :grid

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def place_mark(pos, mark)
    if empty?(pos)
      self[pos] = mark
    else
      raise "position isn't empty"
    end
  end

  def empty?(pos)
    self[pos] == nil
  end

  def winner
    possible_lines = @grid + @grid.transpose +
      [
        [@grid[0][0], @grid[1][1], @grid[2][2]],
        [@grid[0][2], @grid[1][1], @grid[2][0]]
      ]

    possible_lines.each do |line|
      if line == [:X, :X, :X]
        return :X
      elsif line == [:O, :O, :O]
        return :O
      end
    end

    nil
  end

  def over?
    if winner
      return true
    elsif
      @grid.each do |line|
        return false if line.include?(nil)
      end
    end
    true
  end

end
