require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game

  attr_reader :board

  def initialize(player1, player2)
    @board = Board.new
    @player1 = player1
    @player2 = player2
    player1.mark = :X
    player2.mark = :O
    @current_player = @player1
  end

  def current_player
    @current_player
  end

  def switch_players!
    if @current_player == @player1
      @current_player = @player2
    else
      @current_player = @player1
    end
  end

  def play_turn
    pos = @current_player.get_move
    @board.place_mark(pos, @current_player.mark)
    switch_players!
    current_player.display(board)
  end

  def play
    current_player.display(board)

    until @board.over?
      play_turn
    end

    if @board.winner.nil?
      puts "Cats Game"
    else
      current_player.display(board)
      puts "#{@board.winner} is the winner!"
    end
  end

end

if $PROGRAM_NAME == __FILE__
  puts "Enter your name: "
  name = gets.chomp.strip
  human = HumanPlayer.new(name)

  computer = ComputerPlayer.new('computer')

  new_game = Game.new(human, computer)
  new_game.play
end
