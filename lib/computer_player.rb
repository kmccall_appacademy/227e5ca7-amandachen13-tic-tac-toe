class ComputerPlayer

  def initialize(name)
    @name = name
  end

  attr_reader :name
  attr_accessor :board, :mark

  def display(board)
    @board = board
  end

  def get_move
    available_moves = []
    (0..2).each do |row|
      (0..2).each do |col|
        pos = [row, col]
        available_moves << pos if @board.empty?(pos)
      end
    end

    available_moves.each do |pos|
      @board.place_mark(pos, mark)
      if @board.winner
        @board[pos] = nil
        return pos
      else
        @board[pos] = nil
      end
    end

    available_moves.sample
  end

end
